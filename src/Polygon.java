public class Polygon {
    String namaPolygon;
    int tinggi;
    int lebar;


    public String getNamaPolygon() {
        return namaPolygon;
    }

    public void setNamaPolygon(String namaPolygon) {
        this.namaPolygon = namaPolygon;
    }

    public int getTinggi() {
        return tinggi;
    }

    public void setTinggi(int tinggi) {
        this.tinggi = tinggi;
    }

    public int getLebar() {
        return lebar;
    }

    public void setLebar(int lebar) {
        this.lebar = lebar;
    }
}

public class Segitiga extends Polygon implements CommonPolugonMethods
{


    public Segitiga() {
    }

    public Segitiga(int tinggi, int lebar) {
        this.tinggi = tinggi;
        this.lebar = lebar;
    }

    public int luas(int tinggi, int lebar){
        int luas = lebar*tinggi/2;
        return luas;
    }



}
